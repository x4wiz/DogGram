//
// Created by John Pimenov on 10/8/21.
//

import SwiftUI

extension Color {
    struct myTheme {
        static var purpleColor: Color {
            return Color("ColorPurple")
        }
        static var yellowColor: Color {
            return Color("ColorYellow")
        }
        static var beigeColor: Color {
            return Color("ColorBeige")
        }
    }
}