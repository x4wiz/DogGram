//
//  MessageView.swift
//  DogGram (iOS)
//
//  Created by John Pimenov on 10/8/21.
//

import SwiftUI

struct MessageView: View {
    
    @State var comment: CommentModel
    var body: some View {
        HStack {
//            MARK: PROFILE IMAGE
            Image("dog1")
                .resizable()
                .scaledToFill()
                .frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .cornerRadius(20)
            
            VStack(alignment: .leading, spacing: 4, content: {
                // MARK: USERNAME
                Text(comment.userName)
                    .font(.caption)
                    .foregroundColor(.gray)
                //MARK: CONTENT
                Text(comment.content)
                    .padding(.all, 10)
                    .foregroundColor(.primary)
                    .background(Color.gray)
                    .cornerRadius(10)
            })
            Spacer()
        }
    }
}

struct MessageView_Previews: PreviewProvider {
    
    static var comment: CommentModel = CommentModel(commentID: "", userID: "", userName: "John Doe", content: "haha nice!", dateCreated: Date())
    static var previews: some View {
        MessageView(comment: comment)
            .previewLayout(.sizeThatFits)
    }
}
