//
//  SettingsRawView.swift
//  DogGram (iOS)
//
//  Created by John Pimenov on 11/8/21.
//

import SwiftUI

struct SettingsRawView: View {
    
    var leftIcon: String
    var text: String
    var color: Color

    var body: some View {
        HStack {
            ZStack {
                RoundedRectangle(cornerRadius: 8, style: .continuous)
                    .fill(color)
                
                Image(systemName: leftIcon)
                    .font(.title3)
                    .foregroundColor(.white)
            }
            .frame(width: 36, height: 36, alignment: .center)
            
            Text(text)
                .foregroundColor(.primary)
            
            Spacer()
            
            Image(systemName: "chevron.right")
                .font(.headline)
                .foregroundColor(.primary)
        }
        .padding(.vertical, 4)
    }
}

struct SettingsRawView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsRawView(leftIcon: "heart.fill", text: "Basic text", color: .red)
            .previewLayout(.sizeThatFits)
    }
}
