//
//  PostView.swift
//  DogGram (iOS)
//
//  Created by John Pimenov on 10/8/21.
//

import SwiftUI

struct PostView: View {
    
    @State var animateLike: Bool = false
    @State var addHeartAnimationToView: Bool
    @State var post: PostModel
    @State var showActionSheet: Bool = false
    @State var actionSheetType: PostActionSheetOption = .general
    @State var postImage: UIImage = UIImage(named: "dog1")!
    
    enum PostActionSheetOption {
        case general
        case reporting
    }
    
    
    var showHeaderAndFooter: Bool
    
    var body: some View {
        VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 0, content: {
            // MARK: HEADER
            if showHeaderAndFooter {
                HStack {
                    
                    NavigationLink(
                        destination: ProfileView(isMyProfile: false, profileDisplayName: post.username, profileUserID: post.userID),
                        label: {
                            Image("dog1")
                                .resizable()
                                .scaledToFill()
                                .frame(width: 30, height: 30, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                .cornerRadius(15)
                            Text(post.username)
                                .font(.callout)
                                .fontWeight(.medium)
                                .foregroundColor(.primary)
                        })
                    
                    Spacer()
                    
                    Button(action: {
                        showActionSheet.toggle()
                        print("Action sheet pressed")
                    }, label: {
                        Image(systemName: "ellipsis")
                    })
                    .accentColor(.primary)
                    
                    
                    
                }
                .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/, 6)
            }
            //MARK: image
            ZStack {
                Image(uiImage: postImage)
                    .resizable()
                    .scaledToFit()
                if addHeartAnimationToView {
                    LikeAnimationView(animate: $animateLike)
                }
                
            }
            
            // MARK: FOOTER
            
            if showHeaderAndFooter {
                HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 20, content: {
                    
                    Button(action: {
                        
                        if post.likedByUser {
                             // unlike
                            unlikePost()
                        } else {
                            // like
                            likePost()
                        }
                        
                    }, label: {
                        Image(systemName: post.likedByUser ? "heart.fill" : "heart")
                            .font(.title3)
                    })
                    .accentColor(post.likedByUser ? Color.red : Color.primary)
                    .actionSheet(isPresented: $showActionSheet, content: {
                        getActionSheet()
                    })
                    
                    
                    //MARK: COMMENT ICON
                    NavigationLink(
                        destination: CommentsView(),
    //                    isActive: /*@START_MENU_TOKEN@*/.constant(true)/*@END_MENU_TOKEN@*/,
                        label: {
                            Image(systemName: "bubble.middle.bottom")
                                .font(.title3)
                                .foregroundColor(.primary)
                        })
                    Button(action: {
                        sharePost()
                    }, label: {
                        Image(systemName: "paperplane")
                            .font(.title3)
                    })
                    .accentColor(.primary)
                    
                    Spacer()
                })
                .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/, 6)
                
                if let caption = post.caption {
                    HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 10, content: {
                        Text(caption)
                        Spacer(minLength: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/)
                    })
                        .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/, 6)
                }
            }
            
            
        })
    }
    
//     FUNCTIONS
    
    func likePost () {
        // updete the local data
        let updatedPost = PostModel(postID: post.postID, userID: post.userID, username: post.username, caption: post.caption, dateCreated: post.dateCreated, likeCount: post.likeCount + 1, likedByUser: true)
        self.post = updatedPost
        
        animateLike = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            animateLike = false
        }
        
    }
    
    func unlikePost() {
        let updatedPost = PostModel(postID: post.postID, userID: post.userID, username: post.username, caption: post.caption, dateCreated: post.dateCreated, likeCount: post.likeCount - 1, likedByUser: false)
        self.post = updatedPost
    }
    
    func getActionSheet() -> ActionSheet{
        
        switch self.actionSheetType {
        case .general:
            return ActionSheet(title: Text("What would you like to do?"), message: nil, buttons: [
                .destructive(Text("Report"), action: {
                    self.actionSheetType = .reporting
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.showActionSheet.toggle()                    }
                    
                }),
                .default(Text("Learn more..."), action: {
                    print("Learn more pressed")
                }),
                
                .cancel()
            ])
        case .reporting:
            return ActionSheet(title: Text("Why are you reporting this post?"), message: nil,
                buttons: [
                .destructive(Text("This is innapropriate"), action: {
                    reportPost(reason: "This is inappropriate")
                }),
                .destructive(Text("This is spam"), action: {
                    reportPost(reason: "This is spam")
                }),
                .destructive(Text("It made me uncorfortable"), action: {
                    reportPost(reason: "It made me uncorfortable")
                }),
                .cancel({
                            self.actionSheetType = .general
                    
                })
                
            ])
            
        }
        
    }
    func reportPost(reason: String) {
        print("Report post now")
    }
    
    func sharePost() {
        
        let message = "Check out this post"
        let image = postImage
        let link = URL(string: "https://www.google.com")!
        
        
        let activityViewController = UIActivityViewController(activityItems: [message, image, link], applicationActivities: nil)
        
        let viewController = UIApplication.shared.windows.first?.rootViewController
        viewController?.present(activityViewController, animated: true, completion: nil)
    }
}

struct PostView_Previews: PreviewProvider {
    
    static var post: PostModel = PostModel(postID: "", userID: "", username: "User 1", caption: "test caption", dateCreated: Date(), likeCount: 0, likedByUser: false)
    static var previews: some View {
        PostView(addHeartAnimationToView: true, post: post, showHeaderAndFooter: true)
            .previewLayout(.sizeThatFits)
    }
}
