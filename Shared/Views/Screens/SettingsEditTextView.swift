//
//  SettingsEditTextView.swift
//  DogGram (iOS)
//
//  Created by John Pimenov on 11/8/21.
//

import SwiftUI

struct SettingsEditTextView: View {
    @State var submissionText: String = ""
    
    @State var title: String
    @State var description: String
    @State var placeHolder: String
    @Environment(\.colorScheme) var colorScheme


    var body: some View {
        VStack {
            
            HStack {
                Text(description)
                Spacer(minLength: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/)
            }
            
            TextField(placeHolder, text: $submissionText)
                .padding()
                .frame(height: 60)
                .frame(maxWidth: .infinity)
                .background(colorScheme == .light ? Color.myTheme.beigeColor : Color.myTheme.purpleColor)
                .cornerRadius(12)
                .font(.headline)
                .autocapitalization(.sentences)
            
            
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                Text("Save")
                    .font(.title3)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    .padding()
                    .frame(height: 60)
                    .frame(maxWidth: .infinity)
                    .background(colorScheme == .light ? Color.myTheme.purpleColor : Color.myTheme.yellowColor)
                    .cornerRadius(12)
            })
            .accentColor(colorScheme == .light ? Color.myTheme.yellowColor : Color.myTheme.purpleColor)
            
            Spacer()
            
        }
        .navigationBarTitle(title)
        .padding()

    }
}

struct SettingsEditTextView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            SettingsEditTextView(title: "Title", description: "Description", placeHolder: "Placeholder").preferredColorScheme(/*@START_MENU_TOKEN@*/.dark/*@END_MENU_TOKEN@*/)
        }
        
    }
}
