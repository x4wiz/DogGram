//
//  SettingsView.swift
//  DogGram (iOS)
//
//  Created by John Pimenov on 11/8/21.
//

import SwiftUI

struct SettingsView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.colorScheme) var colorScheme

    var body: some View {
        NavigationView {
            ScrollView(/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: false, content: {
                
                // MARK: SECTION 1: DOGGRAM
                GroupBox(label: SettingsLabelView(labelText: "DogGram", labelImage: "dot.radiowaves.left.and.right"), content: {
                    HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 10, content: {
                        Image("logo")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .cornerRadius(12)
                        
                        Text("Doggram is the app for posting pictures of your dog!")
                            .font(.footnote)
                    })
                })
                .padding()
                
                // MARK: SECTION 2: PROFILE
                
                GroupBox(label: SettingsLabelView(labelText: "Profile", labelImage: "person.fill"), content: {
                    NavigationLink(
                        destination: SettingsEditTextView(submissionText: "Current display name", title: "Display name", description: "You can edit your display name here. This will be seen by other users on your account", placeHolder: "Enter new display name"),
                        label: {
                            SettingsRawView(leftIcon: "pencil", text: "Display name", color: Color.myTheme.purpleColor)
                        })
                    
                    NavigationLink(
                        destination: SettingsEditTextView(submissionText: "Current BIO here", title: "Bio", description: "Enter a few words about yourself", placeHolder: "Your bio here..."),
                        label: {
                            SettingsRawView(leftIcon: "text.quote", text: "BIO", color: Color.myTheme.purpleColor)
                        })
                    
                    NavigationLink(
                        destination: SettingEditImageView(title: "Profile Picture", description: "Your profile picture will be shown on your profile and your posts", selectedImage: UIImage(named: "dog1")!),
                        label: {
                            SettingsRawView(leftIcon: "photo", text: "Profile picture", color: Color.myTheme.purpleColor)
                        })
                    
                    SettingsRawView(leftIcon: "figure.walk", text: "Sign out", color: Color.myTheme.purpleColor)
                })
                .padding()
                
                // MARK: SECTION 3: APPLICATION
                
                GroupBox(label: SettingsLabelView(labelText: "Application", labelImage: "apps.iphone"), content: {
                    
                    Button(action: {
                        openCustomURL(urlString: "https://google.com")
                        
                    }, label: {
                        SettingsRawView(leftIcon: "folder.fill", text: "Privacy Policy", color: Color.myTheme.yellowColor)
                    })
                    
                    Button(action: {
                        openCustomURL(urlString: "https://google.com")
                    }, label: {
                        SettingsRawView(leftIcon: "folder.fill", text: "Terms and conditions", color: Color.myTheme.yellowColor)
                    })
                    
                    Button(action: {
                        openCustomURL(urlString: "https://google.com")
                    }, label: {
                        SettingsRawView(leftIcon: "globe", text: "DogGram's website", color: Color.myTheme.yellowColor)
                    })
                    
                })
                .padding()
                
                // MARK: SECTION 4: SIGN OFF
                
                GroupBox {
                    Text("DogGram was made with love. \n All rights reserved \n Cool Apps \n Copyright 2021 ❤️")
                        .foregroundColor(.gray)
                        .multilineTextAlignment(.center)
                        .frame(maxWidth: .infinity)
                }
                .padding()
                .padding(.bottom, 80)
            })
            .navigationBarTitle("Settings")
            .navigationBarTitleDisplayMode(.large)
            .navigationBarItems(leading:
                                    Button(action: {
                                        presentationMode.wrappedValue.dismiss()
                                    }, label: {
                                        Image(systemName: "xmark".lowercased())
                                            .font(.title)
                                    })
                                    .accentColor(.primary)
            )
            
        }
        .accentColor(colorScheme == .light ? Color.myTheme.purpleColor : Color.myTheme.yellowColor)
        
        
    }
    
    // MARK: FUNCTIONS
    
    func openCustomURL(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView().preferredColorScheme(/*@START_MENU_TOKEN@*/.dark/*@END_MENU_TOKEN@*/)
    }
}
