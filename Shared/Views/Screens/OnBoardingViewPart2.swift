//
//  OnBoardingViewPart2.swift
//  DogGram (iOS)
//
//  Created by John Pimenov on 12/8/21.
//

import SwiftUI

struct OnBoardingViewPart2: View {
    
    @State var displayName: String = ""
    @State var showImagePicker: Bool = false
    @State var imageSelected: UIImage = UIImage(named: "logo")!
    @State var sourceType: UIImagePickerController.SourceType = .photoLibrary
    
    var body: some View {
        VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 20, content: {
            Text("What's your name")
                .font(.title)
                .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                .foregroundColor(Color.myTheme.yellowColor)
            
            TextField("Add your name here...", text: $displayName)
                .padding()
                .frame(height: 60)
                .frame(maxWidth: .infinity)
                .background(Color.myTheme.beigeColor)
                .cornerRadius(12)
                .font(.headline)
                .autocapitalization(.sentences)
                .padding(.horizontal)
            
            Button(action: {
                showImagePicker.toggle()
            }, label: {
                Text("Finish: Add profile picture")
                    .font(.headline)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    .padding()
                    .frame(height: 60)
                    .frame(maxWidth: .infinity)
                    .background(Color.myTheme.yellowColor)
                    .cornerRadius(12)
                    .padding(.horizontal)
                
            })
            .accentColor(Color.myTheme.purpleColor)
            .opacity(displayName != "" ? 1.0 : 0.0)
            .animation(.easeInOut(duration: 1.0))
            
        })
        .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color.myTheme.purpleColor)
            .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            .sheet(isPresented: $showImagePicker, onDismiss: createProfile , content: {
                    ImagePicker(imageSelected: $imageSelected, sourceType: $sourceType)
             })
        
    }
    
    func createProfile() {
        print("Create profile")
    }
}

struct OnBoardingViewPart2_Previews: PreviewProvider {
    static var previews: some View {
        OnBoardingViewPart2()
    }
}
