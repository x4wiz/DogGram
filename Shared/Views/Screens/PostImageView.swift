//
//  PostImageView.swift
//  DogGram (iOS)
//
//  Created by John Pimenov on 10/8/21.
//

import SwiftUI

struct PostImageView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.colorScheme) var colorScheme
    
    @State var caption: String = ""
    @Binding var imageSelected: UIImage
    
    var body: some View {
        VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 0, content: {
            HStack {
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                    
                }
                       , label: {
                        Image(systemName: "xmark")
                            .font(.title)
                            .padding()
                })
                .accentColor(.primary)
    
                Spacer()
            }
            
            ScrollView(/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/, content: {
                Image(uiImage: imageSelected)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 200, height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .cornerRadius(12, antialiased: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    .clipped()
                TextField("Add your caption", text: $caption)
                    .padding()
                    .frame(height: 60)
                    .frame(maxWidth: .infinity)
                    .background(colorScheme == .light ? Color.myTheme.beigeColor : Color.myTheme.purpleColor)
                    .cornerRadius(12, antialiased: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    .font(.headline)
                    .padding(.horizontal)
                    .autocapitalization(.sentences)
                    
                
                Button(action: {
                    postPicture()
                }, label: {
                    Text("Post Picture".uppercased())
                        .font(.title3)
                        .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        .padding()
                        .frame(height: 60)
                        .frame(maxWidth: .infinity)
                        .background(colorScheme == .light ? Color.myTheme.purpleColor : Color.myTheme.yellowColor)
                        .cornerRadius(12, antialiased: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                        .font(.headline)
                        .padding(.horizontal)
                    
                })
                .accentColor(colorScheme == .light ? Color.myTheme.yellowColor : Color.myTheme.purpleColor)
                    
            })
        })
    }
    
    // MARK: FUNCTIONS
    
    func postPicture() {
        print("Post Picture to db")
    }
}

struct PostImageView_Previews: PreviewProvider {
    
    @State static var image = UIImage(named: "dog")!
    static var previews: some View {
        PostImageView(imageSelected: $image)
            
    }
}
