//
//  SettingEditImageView.swift
//  DogGram (iOS)
//
//  Created by John Pimenov on 11/8/21.
//

import SwiftUI

struct SettingEditImageView: View {
    
    
    @State var title: String
    @State var description: String
    @State var selectedImage: UIImage // Image shown on this screen
    @State var showImagePicker: Bool = false
    @State var sourceType: UIImagePickerController.SourceType = UIImagePickerController.SourceType.photoLibrary
    

    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            
            HStack {
                Text(description)
                Spacer(minLength: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/)
            }
            
            Image(uiImage: selectedImage)
                .resizable()
                .scaledToFill()
                .frame(width: 200, height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .clipped()
                .cornerRadius(12)
            
            
            Button(action: {
                showImagePicker.toggle()
            }, label: {
                Text("Import")
                    .font(.title3)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    .padding()
                    .frame(height: 60)
                    .frame(maxWidth: .infinity)
                    .background(Color.myTheme.yellowColor)
                    .accentColor(Color.myTheme.purpleColor)
                    .cornerRadius(12)
            })
            .sheet(isPresented: $showImagePicker, content: {
                ImagePicker(imageSelected: $selectedImage, sourceType: $sourceType)
            })
            
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                Text("Save")
                    .font(.title3)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    .padding()
                    .frame(height: 60)
                    .frame(maxWidth: .infinity)
                    .background(Color.myTheme.purpleColor)
                    .accentColor(Color.myTheme.yellowColor)
                    .cornerRadius(12)
            })
            
            Spacer()
            
        }
        .navigationBarTitle(title)
        .padding()

    }
}

struct SettingEditImageView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            SettingEditImageView(title: "String", description: "description", selectedImage: UIImage(named: "dog1")!)
        }
        
    }
}
