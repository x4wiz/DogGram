//
//  ProfileView.swift
//  DogGram (iOS)
//
//  Created by John Pimenov on 11/8/21.
//

import SwiftUI

struct ProfileView: View {
    
    var isMyProfile: Bool
    var posts = PostArrayObject()
    @State var profileDisplayName: String
    var profileUserID: String
    @State var showSettings: Bool = true
    
    @Environment(\.colorScheme) var colorScheme

    var body: some View {
        ScrollView(/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: false, content: {
            ProfileHeaderView(profileDisplayName: $profileDisplayName)
            Divider()
            
            ImageGridView(posts: posts)
        })
        .padding()
        .navigationBarTitle("Profile")
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(trailing:
                                Button(action: {
                                    showSettings.toggle()
                                }, label: {
                                    Image(systemName: "line.horizontal.3")
                                })
                                .accentColor(colorScheme == .light ? Color.myTheme.purpleColor : Color.myTheme.yellowColor)
                                .opacity(isMyProfile ? 1.0 : 0.0)
        )
        .sheet(isPresented: $showSettings, content: {
            SettingsView()
                .preferredColorScheme(colorScheme)
        })
    }
    
}

struct ProfileView_Previews: PreviewProvider {
    
    @State static var name: String = "Joe"
    static var previews: some View {
        NavigationView{
            ProfileView(isMyProfile: true, profileDisplayName: name, profileUserID: "").preferredColorScheme(.dark)
        }
        
    }
}
