//
//  ContentView.swift
//  Shared
//
//  Created by John Pimenov on 9/8/21.
//

import SwiftUI

struct ContentView: View {
    
    @Environment(\.colorScheme) var colorScheme
    
    var currentUserID: String? = nil
    
    var body: some View {
        TabView {
            NavigationView {
                FeedView(posts: PostArrayObject(), title: "Feed")
            }
            
                    .tabItem {
                        Image(systemName: "book.fill")
                        Text("Feed")
                    }
            NavigationView {
                BrowseView()
            }
                    .tabItem {
                        Image(systemName: "magnifyingglass")
                        Text("Feed")
                    }
            UploadView()
                    .tabItem {
                        Image(systemName: "square.and.arrow.up.fill")
                        Text("Upload")
                    }
            
            
            ZStack {
                
                if currentUserID != nil {
                    NavigationView {
                        ProfileView(isMyProfile: true, profileDisplayName: "My profile", profileUserID: "")
                    }
                } else {
                    SignUpView()
                }
            }
                
                
                        .tabItem {
                            Image(systemName: "person.fill")
                            Text("Profile")
                    }
            
        }
        .accentColor(colorScheme == .light ? Color.myTheme.purpleColor : Color.myTheme.yellowColor)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .preferredColorScheme(.dark)
    }
}
