//
//  DogGramApp.swift
//  Shared
//
//  Created by John Pimenov on 9/8/21.
//

import SwiftUI

@main
struct DogGramApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
