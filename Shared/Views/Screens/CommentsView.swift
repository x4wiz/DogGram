//
//  CommentsView.swift
//  DogGram (iOS)
//
//  Created by John Pimenov on 10/8/21.
//

import SwiftUI

struct CommentsView: View {
    
    @Environment(\.colorScheme) var colorScheme
    
    @State var submissionText: String = ""
    @State var commentArray = [CommentModel]()
    
    var body: some View {
        VStack {
            ScrollView {
                LazyVStack{
                    ForEach(commentArray, id: \.self) {comment in
                        MessageView(comment: comment)
                    }
                }
            }
            
            HStack {
                Image("dog1")
                    .resizable()
                    .scaledToFill()
                    .frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .cornerRadius(20)
                
                TextField("Add a comment here", text: $submissionText)
                
                Button(action: {
                    
                }, label: {
                    Image(systemName: "paperplane.fill")
                        .font(.title2)
                })
                .accentColor(colorScheme == .light ? Color.myTheme.purpleColor : Color.myTheme.yellowColor)
                
            }
            .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/, 6)
        }
        .padding(.horizontal)
        .navigationBarTitle("Comments")
        .navigationBarTitleDisplayMode(.inline)
        .onAppear(perform: {
                getComments()
        })
    }
    
    // MARK: functions
    
    func getComments() {
        
        print("Get comments from DB")
        
        let comment1 = CommentModel(commentID: "", userID: "", userName: "Georgia", content: "Whats up", dateCreated: Date())
        let comment2 = CommentModel(commentID: "", userID: "", userName: "Alex", content: "yo", dateCreated: Date())
        let comment3 = CommentModel(commentID: "", userID: "", userName: "Suka naher", content: "Whats up", dateCreated: Date())
        let comment4 = CommentModel(commentID: "", userID: "", userName: "FiorenchettoShaniya", content: "did you fed it?", dateCreated: Date())
        
        self.commentArray.append(comment1)
        self.commentArray.append(comment2)
        self.commentArray.append(comment3)
        self.commentArray.append(comment4)

    }
}

struct CommentsView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            CommentsView().preferredColorScheme(/*@START_MENU_TOKEN@*/.dark/*@END_MENU_TOKEN@*/)
        }
        
    }
}
