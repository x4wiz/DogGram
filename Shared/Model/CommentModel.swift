//
//  CommentModel.swift
//  DogGram (iOS)
//
//  Created by John Pimenov on 10/8/21.
//

import Foundation
import SwiftUI

struct CommentModel: Identifiable, Hashable {
    
    var id = UUID()
    var commentID: String
    var userID: String
    var userName: String
    var content: String
    var dateCreated: Date
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
}

