//
//  LaunchScreen.swift
//  DogGram
//
//  Created by John Pimenov on 9/8/21.
//

import SwiftUI

extension Color {
    static let ui = Color.UI()
    
    struct UI {
         let yellow = Color("ColorYellow")
    }
}

struct LaunchScreen: View {
    var body: some View {
        Color.ui.yellow
            .ignoresSafeArea()
            .overlay(
                VStack(alignment: .center) {
                    Image("logo.transparent").resizable().frame(width: 150, height: 150)
                        
                    Text("Welcome to DogGram")
                    
                }
            )
    }
}

struct LaunchScreen_Previews: PreviewProvider {
    static var previews: some View {
        LaunchScreen()
    }
}
